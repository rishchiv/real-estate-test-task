import { Component, Input } from '@angular/core';
import { House } from '../../models';

// TODO(5pts): render house

@Component({
  selector: 'app-house-card',
  templateUrl: 'house-card.component.html',
  styles: []
})
export class HouseCardComponent {
  @Input() house: House;
}
