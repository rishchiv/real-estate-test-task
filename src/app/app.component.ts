import { Component } from '@angular/core';

@Component({
  selector: 'app-root',
  template: `
    <div class="main-container">
      <router-outlet></router-outlet>
    </div>  
    `,
  styles: [`
  .main-container {
    width: 60%;
    margin: 25px auto
  }
  `]
})
export class AppComponent { }
