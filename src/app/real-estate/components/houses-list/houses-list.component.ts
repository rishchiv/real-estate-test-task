import { Component, Input } from '@angular/core';
import { House } from '../../models';

/* TODO(5pts): render houses list */
@Component({
  selector: 'app-houses-list',
  templateUrl: './houses-list.component.html',
  styleUrls: ['./houses-list.component.css']
})
export class HousesListComponent {

  @Input() houses: House[];
  displayedColumns: string[] = ['id', 'title', 'price', 'onsale'];

}
