import { Component, OnInit } from '@angular/core';
import { ActivatedRoute, Params } from '@angular/router';
import { Observable } from 'rxjs';
import { HousesService } from '../apis/houses.service';
import { House } from '../models';
import { switchMap } from 'rxjs/operators';

@Component({
  selector: 'app-house-details',
  template: `
  <app-house-card [house]="house$ | async" data-test-house-card></app-house-card>
  `,
  styles: [
    `
      :host {
        display: block;
      }
    `
  ]
})
export class HouseDetailsComponent implements OnInit {
  house$: Observable<House>;

  constructor(
    private activatedRoute: ActivatedRoute,
    private houseAPI: HousesService
  ) { }

  ngOnInit() {
    /* TODO(3pts)
      Goal: fetch house based on the activated route's :id
    */
    this.house$ = this.activatedRoute.params.pipe(
      switchMap(params => this.houseAPI.getHouse(+params['id']))
    )
  }
}
