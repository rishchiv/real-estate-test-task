import { Component, OnInit } from '@angular/core';
import { ActivatedRoute, Router, Params } from '@angular/router';
import { Observable, combineLatest, of } from 'rxjs';
import { CitiesService } from '../apis/cities.service';
import { HousesService } from '../apis/houses.service';
import { City, House, HouseFilters } from '../models';
import { map, switchMap } from 'rxjs/operators';

@Component({
  selector: 'app-house-listings',
  template: `
    <app-houses-filters 
      [cities]="cities$ | async" 
      [filters]="filters$ | async" 
      (filtersChange)="onFiltersChange($event)">
    </app-houses-filters>

    <app-houses-list [houses]="houses$ | async">
    </app-houses-list>
  `,
  styles: [
    `
      : host {
        display: block;
      }
    `
  ]
})
export class HouseListingsComponent implements OnInit {
  cities$: Observable<City[]>;
  filters$: Observable<HouseFilters>;
  houses$: Observable<House[]>;

  constructor(
    private citiesAPI: CitiesService,
    private houseAPI: HousesService,
    private activatedRoute: ActivatedRoute,
    private router: Router
  ) { }

  ngOnInit() {

    // 1.TODO(1pts)
    // Goal: fetch all cities
    // Implementation: set this.cities$
    this.cities$ = this.citiesAPI.getCities();

    // 2. TODO(5pts)
    //   Goal: parse query params stream into filters object
    //   Implementation: set this.filters$
    //   Hint:
    //     query params: real-estate?cityId=3&onSale=false&priceLessThan=244
    //     parsed value: { cityId: 3, onSale: false, priceLessThan: 244 }

    this.filters$ = combineLatest(
      this.activatedRoute.params,
      this.activatedRoute.queryParams
    ).pipe(
      map(([params, queryParams]) => ({ ...params, ...queryParams })),
      switchMap((params: Params) => {
        for (let propName in params) {
          if (propName == 'cityId' || propName == '')
            params[propName] = +params[propName];
          else if (propName == 'onSale')
            params[propName] = params[propName] == 'true' ? true : false;
        }
        return of(params);
      })
    )

    // 3. TODO(8pts)
    //   Goal: fetch all houses matching current filters
    //   Implementation: set this.houses$
    //   Hint: this example includes using higher order observables.
    //         we must switch from the filters$ stream into the houses$ stream.

    this.houses$ = this.filters$.pipe(
      switchMap((params: HouseFilters) => this.houseAPI.getHouses(params))
    );
  }

  onFiltersChange(queryParams: HouseFilters) {
    /* TODO(1pts)
      Goal: update URL query params with the new filters
    */
    queryParams = Object.assign({}, queryParams);
    this.router.navigate(['.'], { queryParams: queryParams });
  }

}
