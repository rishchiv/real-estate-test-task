import { async, ComponentFixture, TestBed } from '@angular/core/testing';
import { HousesListComponent } from './houses-list.component';

import { MatTableModule } from '@angular/material';
import { RouterModule } from '@angular/router';
import { HousesService } from '../../apis/houses.service';

import { By } from '@angular/platform-browser';
import { HOUSES } from '../../apis/houses.data';

describe('HousesListComponent', () => {
  let component: HousesListComponent;
  let fixture: ComponentFixture<HousesListComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [HousesListComponent],
      imports: [
        MatTableModule,
        RouterModule
      ]
    }).compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(HousesListComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  // TODO(3pts)
  it('should render list of houses', async(() => {

  }));
});
