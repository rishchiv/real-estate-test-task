import { async, ComponentFixture, TestBed } from '@angular/core/testing';
import { House } from '../../models';
import { HouseCardComponent } from './house-card.component';
import {
  MatCardModule
} from '@angular/material';

describe('HouseCardComponent', () => {
  let component: HouseCardComponent;
  let fixture: ComponentFixture<HouseCardComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [HouseCardComponent],
      imports: [
        MatCardModule
      ]
    }).compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(HouseCardComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  // // TODO(1pts)
  // it('should render house title', () => {
  // });

  // // TODO(1pts)
  // it('should render card description', () => {
  //   expect(component.house.description).toBeDefined();
  // });

  // // TODO(1pts)
  // it('should render card image', () => {
  //   expect(component.house.image).toBeDefined();
  // });

  // // TODO(1pts)
  // it('should render house price', () => {
  //   expect(component.house.price).toBeDefined();
  // });

  // // TODO(1pts)
  // it('should render house onSale if house is on sale', () => {
  //   component.house.onSale
  //   expect(component.house.onSale).toBeDefined();
  // });

  // TODO(1pts)
  // it('should NOT render house onSale if house is not on sale', () => { });
});
