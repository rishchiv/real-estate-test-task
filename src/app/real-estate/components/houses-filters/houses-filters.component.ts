import { Component, EventEmitter, Input, Output, OnInit } from '@angular/core';
import { FormBuilder, FormGroup, FormControl } from '@angular/forms';
import { City, HouseFilters } from '../../models';

/* TODO(5pts): create form controls */
/* TODO(5pts): render form */

@Component({
  selector: 'app-houses-filters',
  templateUrl: './houses-filters.component.html',
  styleUrls: ['./houses-filters.component.css']
})
export class HousesFiltersComponent implements OnInit {

  filterForm: FormGroup;

  @Input() cities: City[];

  @Input()
  set filters(v: HouseFilters) {
    this.filterForm.patchValue(v || {});
  };

  @Output() filtersChange = new EventEmitter<HouseFilters>();

  constructor(private fb: FormBuilder) {
    this.createForm();
  }

  ngOnInit() { }

  createForm() {
    this.filterForm = this.fb.group({
      cityId: new FormControl(null),
      priceLessThan: new FormControl(null),
      onSale: new FormControl(null)
    })
  }

  onSubmit() {
    for (let propName in this.filterForm.value) {
      if (this.filterForm.value[propName] == null)
        delete this.filterForm.value[propName];
    }
    this.filtersChange.emit(this.filterForm.value);
  }
}
