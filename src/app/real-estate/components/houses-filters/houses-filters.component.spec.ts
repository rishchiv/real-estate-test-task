import { async, ComponentFixture, TestBed } from '@angular/core/testing';
import { FormGroup, ReactiveFormsModule, FormControl } from '@angular/forms';
import { HousesFiltersComponent } from './houses-filters.component';
import { BrowserAnimationsModule } from '@angular/platform-browser/animations';
import { By } from '@angular/platform-browser';

import {
    MatExpansionModule,
    MatSelectModule,
    MatInputModule,
    MatButtonModule,
    MatRadioModule,
    MatFormFieldModule
} from '@angular/material';

describe('HousesFiltersComponent', () => {
    let component: HousesFiltersComponent;
    let fixture: ComponentFixture<HousesFiltersComponent>;

    beforeEach(async(() => {
        TestBed.configureTestingModule({
            declarations: [HousesFiltersComponent],
            imports: [
                ReactiveFormsModule,
                MatExpansionModule,
                MatSelectModule,
                MatRadioModule,
                MatFormFieldModule,
                MatInputModule,
                MatButtonModule,
                BrowserAnimationsModule
            ]
        }).compileComponents();
    }));

    beforeEach(() => {
        fixture = TestBed.createComponent(HousesFiltersComponent);
        component = fixture.componentInstance;
        fixture.detectChanges();
    });

    it('should create filters form', () => {
        expect(component.filterForm instanceof FormGroup).toBe(true);
    });

    // TODO(1pts)
    it('form should have cityId control', () => {
        expect(component.filterForm.controls['cityId'] instanceof FormControl).toBe(true);
    });

    // TODO(1pts)
    it('should render cityId control', () => {
        const citySelector = fixture.debugElement.query(
            By.css('mat-select[formControlName=cityId]'));
        expect(citySelector).toBeDefined();
    });

    // TODO(1pts)
    it('form should have priceLessThan control', () => {
        expect(component.filterForm.controls['priceLessThan'] instanceof FormControl).toBe(true);
    });

    // TODO(1pts)
    it('should render priceLessThan control', () => {
        const input = fixture.debugElement.query(By.css('input[formControlName=priceLessThan]'))
        expect(input.nativeElement).toBeDefined();
    });

    // TODO(1pts)
    it('form should have onSale control', () => {
        expect(component.filterForm.controls['onSale'] instanceof FormControl).toBe(true);
    });

    // TODO(1pts)
    it('should render onSale control', () => {
        const onSaleRadioGroup = fixture.debugElement.query(
            By.css('mat-radio-group[formControlName=onSale]'));

        expect(onSaleRadioGroup).toBeDefined();
        expect(onSaleRadioGroup.childNodes.length).toBe(3);
    });
});
